(function(){
    //estableciendo que este script se ejecutara cuando el documento este cargado
    document.addEventListener("DOMContentLoaded", function(){

    //bloqueando el evento de enviar para que no se ejecute desde un lugar diferente al scripts
        $.validator.setDefaults({
            submitHandler: function(){
                $("#formcategory").submit(); //opteniendo el id del elemento que se validara en este caso el formulario que tiene como id formusers
            }
        });
        $("#formcategory").validate({
            rules:{
                name:{required:true, maxlength:25, minlength:3},
                Descripcion:{required:true, maxlength:70, minlength:15}

            },
            messages:{
                name:{
                    required:"Es necesario completar el campo nombre para realizar el proceso, por favor hacerlo",
                    maxlength: "Su nombre no puede ser mayor a 25 caracteres",
                    minlength: "Su nombre no puede ser menor a 3 caracteres",

                },

                Descripcion:{
                    required:"Es necesario completar el campo Descripcion para realizar el proceso, por favor hacerlo",
                    maxlength: "Su Descripcion no puede ser mayor a 70 caracteres",
                    minlength: "Su Descripcion no puede ser menor a 15 caracteres",

                }
            },
            errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          //optenemos el elemento padre en donde se incluiran los demas elementos de notificacion
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
            
        });
    
    });
    
}());