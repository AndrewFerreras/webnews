(function () {
//estableciendo que este script se ejecutara cuando el documento este cargado
document.addEventListener("DOMContentLoaded", function(){

    //bloqueando el evento de enviar para que no se ejecute desde un lugar diferente al scripts
    $.validator.setDefaults({
        submitHandler: function(){
            $("#formusers").submit(); //opteniendo el id del elemento que se validara en este caso el formulario que tiene como id formusers
        }
    });
    //accediento al formulario mediante su id y aplicando la funccion validate
    $("#formusers").validate({
        //establecimiento de los parametros a seguir para hacer la validacion en rules
        //name,role,email etc son elementos hijos que pertenecen al formulario de validacion por lo cual podemos acceder a ellos por medio de su nombres
        rules:{
            name:{
                //cada uno de lass metodos aplicados aca estan en la documentacion de https://jqueryvalidation.org/ en la seccion de metodos
                
                required:true,
                maxlength:25,
                minlength:3
            },
            role:{
                required:true,
                number:true,
                range: [1, 3]
                
            },
            email:{
                required:true,
                email: true

            },
            password:{
                required:true,
                maxlength:16,
                minlength:8
            },
            pass2:{
                required:true,
                maxlength:16,
                minlength:8,
                equalTo:"#password"
            }

        },
        //messages esta propiedad establece mensajes personalizados para las cada uno de los casos expecificados en rules, 
        //sin esta propiedad la validaccion funcionaria generando mensajes por defectos
        messages:{
            name:{
                required:"Por favor introducir su nombre para completar el registro",
                maxlength:"Su nombre no puede ser mayor a 25 caracteres",
                minlength:"Su nombre no puede ser menor a tres caracteres"
            },
            role:{
                required:"Por favor seleccionar el rol asignado para completar el registro",
                number:"por favor seleccionar un rol de la lista",
                range: "por favor seleccionar un rol de la lista"
                
            },
            email:{
                required:"Por favor rellenar campo email",
                email: "Su email no es valido verifique nuevamente"

            },
            password:{
                required:"por favor complete el campo contraseña es necesario establecerla",
                maxlength:"Su contraseña como maximo debe tener 16 caracteres",
                minlength:"Su contraseña debe tener como minimo 8 caracteres"
            },
            pass2:{
                required:"Es necesario que repita la contraseña, para continuar",
                maxlength:"Su contraseña como maximo debe tener 16 caracteres",
                minlength:"Su contraseña debe tener como minimo 8 caracteres",
                equalTo:"Ambas contraseñas deben se iguales para poder realizar el registro"
            },
        },
        // aca creamos secciones dentro del formulario en donde se visualizaran los errores si es que alguno existe
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          //optenemos el elemento padre en donde se incluiran los demas elementos de notificacion
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }

        
    });
});



}());