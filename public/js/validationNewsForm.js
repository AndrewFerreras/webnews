(function(){
    document.addEventListener('DOMContentLoaded', function(){
        $.validator.setDefaults({
            submitHandler: function(){
                $("#formnews").submit();
            }
        });
        $("#formnews").validate({
           
            rules:{
                title:{
                    required:true,
                    maxlength:100,
                    minlength:12
                    
                },
                Category:{
                    required: true,
                    number: true
                },
                Urlphoto:{
                    required: true,
                    url: true
                },
               
    
    
            },
            messages:{
                title:{
                    required: "Por favor introducir el titulo o descripcion que tendra este articulo",
                    maxlength: "este titulo es un poco largo, favor intentar reducirlo",
                    minlength:"este titulo es un poco corto, favor intenta describir mas"
                },
                Category:{
                    required: "Debes seleccionar una categoria para relacionar los articulos si no existe la indicada favor crearla.",
                    number:"debes seleccionar una de las categorias provistas en las opciones",
                },
                Urlphoto:{
                    required: "Los recursos visuales son de gran ayuda para poner en contexto adjunta el link de alguna",
                    url: "Lo siento esto no es un url valido intenta con otro"
                },

              
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
              error.addClass('invalid-feedback');
              //optenemos el elemento padre en donde se incluiran los demas elementos de notificacion
              element.closest('.form-group').append(error);
              element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
              $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
            }
        });
    });
  
}());