<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;
        $APP_URL="localhost:8000";
        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                if($_SERVER['HTTP_HOST']=="${APP_URL}"){
                    return redirect(RouteServiceProvider::HOME);
                }
                if (Auth::guard($guard)->check() and $_SERVER['HTTP_HOST']=="app.{$APP_URL}") {
                    return redirect(RouteServiceProvider::DASHBOARD);
        
                    
                }
                
            }
        }

        return $next($request);
    }
}
