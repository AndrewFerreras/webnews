<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;

class Loginrole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(isset(Auth::user()->role)&& Auth::user()->role==3){
            
            return $next($request) ;
        }

        if(isset(Auth::user()->role)&& Auth::user()->role==2){
            
            return redirect()->route ('editor.dashboard');
        }
        
        return redirect()->route ('notfound');
    }
}
