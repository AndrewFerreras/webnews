<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        $APP_URL="app.localhost:8000";
        $APP_URLCLIENT="localhost:8000";
        if (! $request->expectsJson() and $_SERVER['HTTP_HOST']== "${APP_URL}") {
            return  route('loginad');

            
        }

        if ($request->expectsJson() and $_SERVER['HTTP_HOST']=="app.localhost:8000") {
            return  route('loginad');

            
        }

        if (! $request->expectsJson()) {

            if( $_SERVER['HTTP_HOST']=="${APP_URLCLIENT}")
            {
                return route('login');

            }
                

        }
    }
}
