<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Roles;

class Usercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = \DB::table('users')->select('id','name','email','role')->where('active', '!=',0)->where('role', '!=',1)->get();
        return view("backofice.admin.users.index", compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = \DB::table('roles')->select('id','name')->get();
        return view("backofice.admin.users.create", compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $users)
    {
        
        $users = new User($this->validateFields($request));
        $users->password=bcrypt( $users->password);
        $users->save();
        $request->session()->flash("flash_message","El usuario fue creado de manera satisfactoria!");
        return redirect('/administrador/users');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = $this->GetUsers($id);
        return view("backofice.admin.users.show", compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = \DB::table('roles')->select('id','name')->get();
        $users = $this->GetUsers($id);
        return view("backofice.admin.users.edit", compact('users','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $users)
    {
        $datos=$this->validateFieldsEdit($request);
        $datos['password']= bcrypt($datos['password']);
        \DB::table('users')->where('id','=',$datos['id'])->update(
            ['name'=>$datos['name'],'email'=>$datos['email'],'role'=>$datos['role'],
             'password'=>$datos['password']]
        );
        
        $request->session()->flash("flash_message","El usuario fue actualizado de manera satisfactoria!");


        return redirect("administrador/users");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('users')->where('id','=',$id)->update(['active'=>0]);
        request()->session()->flash("flash_message","El usuario fue eliminado de manera satisfactoria!");


        return redirect("administrador/users");
    }

    public function validateFields(Request $request){

        $Validatedata= $request->validate(
            ['name'=>'required|min:3|max:16',
             'email'=>'required|email|unique:users,email',
             'role'=>'required|numeric',
             'password'=>'required|min:8|max:16',
             'pass2'=>'required|same:password'],
        
            [
                'name.required'=> 'Por favor introducir su nombre para completar el registro',
                'name.max'=>'Su nombre no puede ser mayor a 25 caracteres',
                'name.min'=>'Su nombre no puede ser menor a tres caracteres',
                'email.required'=>'Por favor rellenar campo email',
                'email.email'=>'Su email no es valido verifique nuevamente',
                'email.unique'=>'este email ya esta registrado en la base de datos',
                'role.required'=>'Por favor seleccionar el rol asignado para completar el registro',
                'role.numeric'=>'por favor seleccionar un rol de la lista',
                'password.required'=>'por favor complete el campo contraseña es necesario establecerla',
                'password.max'=>'Su contraseña debe tener como maximo 16 caracteres',
                'password.min'=>'Su contraseña debe tener como minimo 8 caracteres',
                'pass2.required'=>'Es necesario que repita la contraseña, para continuar',
                'pass2.same'=>'Ambas contraseñas deben se iguales para poder realizar el registro'
                
            ]);
        return $Validatedata;
    }


    public function GetUsers($id){
        $users = \DB::table('users')->select('id','name','email','role')->where('id', '=',$id,'and','active','!=',1)->get();
        return $users;
    }


    public function validateFieldsEdit(Request $request){

        $Validatedata= $request->validate(
            ['id'=>'required',
            'name'=>'required|min:3|max:25',
             'email'=>'required|email',
             'role'=>'required|numeric',
             'password'=>'required|min:8|max:16',
             'pass2'=>'required|same:password'],
        
            [
                'name.required'=> 'Por favor introducir su nombre para completar el registro',
                'name.max'=>'Su nombre no puede ser mayor a 25 caracteres',
                'name.min'=>'Su nombre no puede ser menor a tres caracteres',
                'email.required'=>'Por favor rellenar campo email',
                'email.email'=>'Su email no es valido verifique nuevamente',
                'role.required'=>'Por favor seleccionar el rol asignado para completar el registro',
                'role.numeric'=>'por favor seleccionar un rol de la lista',
                'password.required'=>'por favor complete el campo contraseña es necesario establecerla',
                'password.max'=>'Su contraseña debe tener como maximo 16 caracteres',
                'password.min'=>'Su contraseña debe tener como minimo 8 caracteres',
                'pass2.required'=>'Es necesario que repita la contraseña, para continuar',
                'pass2.same'=>'Ambas contraseñas deben se iguales para poder realizar el registro'
                
            ]);
        return $Validatedata;
    }

}
