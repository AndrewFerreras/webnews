<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Categorias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psy\CodeCleaner\FunctionContextPass;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $categorys= Categorias::get();
       return view('backofice.admin.category.index', compact('categorys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backofice.admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user=Auth::user()->id;
       $datos=$this->validateFields($request);
       \DB::insert('insert into categorias (name,description,create_at) values (?,?,?)', [$datos['name'],
       $datos['Descripcion'],$user]);
       $request->session()->flash("flash_message","La Categoria fue creada de manera satisfactoria!");
       return redirect('/administrador/categorys');

       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function show( $categorias)
    {
        $categorys=\DB::table('categorias')->select('id','name','description','create_at','update_at',
        'created_at','updated_at')->where('id','=',$categorias)->get();
        $users=User::get();

        return view('backofice.admin.category.show',compact('users','categorys'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function edit( $categorias)
    {
        $categorys=\DB::table('categorias')->select('id','name','description')->where('id','=',$categorias)->get();
        return view('backofice.admin.category.edit',compact('categorys'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categorias $categorias)
    {
       $categorias=$this->validateFieldsEdit($request);

       $user=Auth::user()->id;
       \DB::table('categorias')->where('id','=',$categorias['id'])->update([
           'name'=>$categorias['name'],'description'=>$categorias['Descripcion'],
           'update_at'=>$user
       ]);

       return view('backofice.admin.editor.dashboard',compact('categorias','user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categorias $categorias)
    {
        //
    }


    public function validateFields(Request $request){
        $validedata= $request->validate([
            'name'=>'required|min:3|max:25',
            'Descripcion'=>'required|min:15|max:70'

        ],
        [
            'name.required'=>'Es necesario completar el campo nombre para realizar el proceso, por favor hacerlo',
            'name.max'=>'Su nombre no puede ser mayor a 25 caracteres',
            'name.min'=>'Su nombre no puede ser menor a 3 caracteres',
            'Descripcion.required'=>'Es necesario completar el campo Descripcion para realizar el proceso, por favor hacerlo',
            'Descripcion.max'=>'Su Descripcion no puede ser mayor a 70 caracteres',
            'Descripcion.min'=>'Su Descripcion no puede ser menor a 15 caracteres'
        ]
        );
        return $validedata;
    }
    public function validateFieldsEdit(Request $request){
        $validedata= $request->validate([
            'id'=>'required',
            'name'=>'required|min:3|max:25',
            'Descripcion'=>'required|min:15|max:70'

        ],
        [
            'name.required'=>'Es necesario completar el campo nombre para realizar el proceso, por favor hacerlo',
            'name.max'=>'Su nombre no puede ser mayor a 25 caracteres',
            'name.min'=>'Su nombre no puede ser menor a 3 caracteres',
            'Descripcion.required'=>'Es necesario completar el campo Descripcion para realizar el proceso, por favor hacerlo',
            'Descripcion.max'=>'Su Descripcion no puede ser mayor a 70 caracteres',
            'Descripcion.min'=>'Su Descripcion no puede ser menor a 15 caracteres'
        ]
        );
        return $validedata;
    }

    
}
