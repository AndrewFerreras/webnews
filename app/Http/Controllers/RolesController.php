<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = \DB::table('roles')->select('id','name','description')->get();
        //las consultas por defectos de laravel trae consigo los datos en json lo que significa muchos datos que son de caracter delicado es mejor realizar consultas personalizadas para solo traer los datos en bruto necesarios

        return view('backofice.admin.role.index',compact("roles"));
    }


   
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function show( $roles)
    {
     
        $id=$roles;
        $roles = \DB::table('roles')->select('id','name','description')->where('id', '=',$id)->get();
       return  view('backofice.admin.role.show',compact("roles"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Roles  $roles
     * @return \Illuminate\Http\Response
     */
   
   
}
