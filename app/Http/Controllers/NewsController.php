<?php

namespace App\Http\Controllers;

use App\Models\news;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use League\CommonMark\Extension\Table\Table;


class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = \DB::table('news')->where('status','=','1')->select('id','title','created_at','updated_at')->get();
        return view("backofice.admin.news.index",compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Categorys= \DB::table('categorias')->select('id', 'name')->get();
        return view("backofice.admin.news.create", compact('Categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user=Auth::user()->id;
         $datos = $this->ValidateFiels($request);
         \DB::table('news')->insert(['categoria'=> $datos['Category'],'title'=> $datos['title'],
                                     'image'=> $datos['Urlphoto'],'autor'=>$user                                                          
         ]);
         $articleID = \DB::table('news')->max('id');
         \DB::table('article')->insert(['body'=>$datos['Article'],'news-id'=>$articleID]);
    }     

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\news  $news
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories= $this->GetCategories();
        $users=\DB::table('users')->select('name','id')->get();
        $news = \DB::table('news')->where('status','=','1')->where('id','=',$id)->select('id','title','created_at','updated_at','autor',
            'categoria','image','update_for')->get();
        return view("backofice.admin.news.show",compact('users','news','categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\news  $news
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories= $this->GetCategories();
        $news= \DB:: table('news')->where('status','=','1')->where('news.id','=',$id)->join('article', 'article.id','=','news.id')
        ->select('news.title','news.id','news.image','article.body')->get();  
        return view("backofice.admin.news.edit" ,compact('categories','news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\news  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user=Auth::user()->id;
        $date = now();

         $datos = $this->ValidateFielsedit($request);
         \DB::table('news')->where('status','=','1')->where('id','=',$datos['id'])->update(['categoria'=> $datos['Category'],'title'=> $datos['title'],
                                     'image'=> $datos['Urlphoto'],'update_for'=>$user,'updated_at'=>$date                                                         
         ]);
         \DB::table('article')->where('id','=',$datos['id'])->update(['body' => $datos['Article']]);

       return redirect('/administrador/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\news  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        \DB::table('news')->where('id','=',$id)->update(['status'=>'0']);
        return redirect('/administrador/news');
    }


    public function ValidateFiels(Request $request){

      
        $Rules=  [
            'title'=>'required|max:100|min:12',
            'Category'=>'required|numeric',
            'Urlphoto'=>'required|url',
            'Article'=>'required|max:6000|min:700',
        ];

        $Message = [
            'id.requiered'=>'Es necesario obtener el id',
            'id.numeric'=>'Es necesario que el id ya este registrado en la base de datos',
            'title.required'=>'Por favor introducir el titulo o descripcion que tendra este articulo',
            'title.max'=>'este titulo es un poco largo, favor intentar reducirlo',
            'title.min'=>'este titulo es un poco corto, favor intenta describir mas',
            'Category.required'=>'Debes seleccionar una categoria para relacionar los articulos si no existe la indicada favor crearla',
            'Category.numeric'=>'debes seleccionar una de las categorias provistas en las opciones',
            'Urlphoto.required'=>'Los recursos visuales son de gran ayuda para poner en contexto adjunta el link de alguna',
            'Urlphoto.url'=>'Lo siento esto no es un url valido intenta con otro',
            'Article.required'=>'Por favor introducir el cuerpo de este articulo',
            'Article.max'=>'este Articulo es un poco largo, favor intentar reducirlo',
            'Article.min'=>'este Articulo es un poco corto, favor intenta describir mas'

        ];

       
        
            $validateData = $request->validate( $Rules,$Message);
        
        return $validateData;
    }

    public function ValidateFielsedit(Request $request){

      
        $Rules=  [
            'id'=>'required|numeric',
            'title'=>'required|max:100|min:12',
            'Category'=>'required|numeric',
            'Urlphoto'=>'required|url',
            'Article'=>'required|max:6000|min:700',
        ];

        $Message = [
            'id.requiered'=>'Es necesario obtener el id',
            'id.numeric'=>'Es necesario que el id ya este registrado en la base de datos',
            'title.required'=>'Por favor introducir el titulo o descripcion que tendra este articulo',
            'title.max'=>'este titulo es un poco largo, favor intentar reducirlo',
            'title.min'=>'este titulo es un poco corto, favor intenta describir mas',
            'Category.required'=>'Debes seleccionar una categoria para relacionar los articulos si no existe la indicada favor crearla',
            'Category.numeric'=>'debes seleccionar una de las categorias provistas en las opciones',
            'Urlphoto.required'=>'Los recursos visuales son de gran ayuda para poner en contexto adjunta el link de alguna',
            'Urlphoto.url'=>'Lo siento esto no es un url valido intenta con otro',
            'Article.required'=>'Por favor introducir el cuerpo de este articulo',
            'Article.max'=>'este Articulo es un poco largo, favor intentar reducirlo',
            'Article.min'=>'este Articulo es un poco corto, favor intenta describir mas'

        ];

       
        
            $validateData = $request->validate( $Rules,$Message);
        
        return $validateData;
    }
    public function GetCategories(){
        $categories= \DB::table('categorias')->select('id', 'name')->get();
        return  $categories; 
    }
}
