<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
            'id'=>1,
            'name'=>'Visitante' ,
            'description'=>'Cliente final el cual solo tiene permisos de lectura de las noticias y se filtran por las categorias seleccionadas'
        ]);

        \DB::table('roles')->insert([
            'id'=>2,
            'name'=>'Editor',
            'description'=>'Encargado de Crear noticias y editarlas el mismo no puede eliminarlas ni tiene acceso a los demas modulos administrativos'
        ]);

        \DB::table('roles')->insert([
            'id'=>3,
            'name'=>'Administrador',
            'description'=>'encargado de realizar todos los monitoreos usuario que goza de todos los privilegios y permisos'
        ]);
        
    }
}
