<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->unsignedBigInteger('categoria')->index();
            $table->string('image');
            $table->unsignedBigInteger('autor')->index();
            $table->timestamps();
            $table->unsignedBigInteger('update_for')->index();
            $table->foreign('categoria')->references('id')->on('categorias');
            $table->foreign('autor')->references('id')->on('users');
            $table->tinyInteger('status')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
