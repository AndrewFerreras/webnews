<?php

use App\Http\Controllers\CategoriasController;
use App\Http\Controllers\NewsController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use App\Http\Controllers\Usercontroller;
use App\Http\Controllers\RolesController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$APP_URL=env('APP_URL');


///Rutas con subdominio app
Route::domain("app.${APP_URL}")->group(function () {
        

        Route::middleware(['auth:sanctum', 'verified'])->group(function () {
            Route::get('administrador/dashboard', function () {
                //la ruta esta funcionando correctamente
                return view('backofice.admin.dashboard');
            })->name('administrador.dashboard')->middleware(['role']);

            Route::get('Editor/dashboard', function () {
                //la ruta esta funcionando correctamente
                return view('backofice.admin.editor.dashboard');
            })->name('editor.dashboard');
            
            Route::get('/denied', function() {
                return view ('backofice.errors.401');
            })->name('denied');

            Route::get('/internar-error', function() {
                return view ('backofice.errors.500');
            })->name('internal-error');

            Route::resource('/administrador/users',Usercontroller::class)->middleware(['roleResource']);
            Route::resource('/administrador/roles',RolesController::class)->middleware(['roleResource']);
            Route::resource('/administrador/categorys',CategoriasController::class)->middleware(['roleResource']);
            Route::resource('/administrador/news',NewsController::class);

        });
        Route::get('/', function () {
            return view('auth/loginad');
        })->name('loginad');
        Route::get('/login', function () {
            return view('auth/loginad');
        })->name('loginad2');

    

    Route::get('/notfound', function() {
        return view ('backofice.errors.404');
    })->name('notfound');

   

    

   
});

//rutas del dominio principal


Route::domain("${APP_URL}")->group(function () {
Route::get('manage/dashboard', function () {
        
    return view('backofice.admin.dashboard');
});

Route::get('/', function () {
    return view('welcome');
})->name('main-page');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

});






