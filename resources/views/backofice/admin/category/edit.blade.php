
@extends('layouts.admin')


@section('main_content')
<h1 class="mt-4">Categorias</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">En este recurso puedes crear nuevas Categorias para administrar y gestionar las noticias</li>
</ol>
<div class="card mb-4">
  
</div>
@foreach ($errors->all() as $error)
<div class="alert alert-danger alert-dismissible fade show" role="alert" id="error" >
  <strong>Error! </strong> {{ $error }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endforeach

  
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
          <h2>Editar Categoria </h2>
      </div>
      <div class="card-body">
        <form role="form" id="formcategory" name="category" method="post" action="{{ url('/administrador/categorys/'.$categorys[0]->id)}}">
            @method('PATCH')
            @csrf
            <input type="hidden" value="{{$categorys[0]->id}}" name="id" id="id"/>
            <div class="form-group">
                <div class="controls">
                    <label for="name"> Nombre de la Categoria: </label>
                    <input class="form-control"  type="text" name="name" id="name" placeholder="Introducir Categoria."
                    value="{{$categorys[0]->name}}"/>
                   
                </div>
            </div>
            
            <div class="form-group">
                <div class="controls">
                    <label for="Descripcion"> Descripcion: </label>
                    <input class="form-control"  type="text" name="Descripcion" id="Descripcion" placeholder="Introducir Descripcion."
                    value="{{$categorys[0]->description}}" />
                   
                </div>
            </div>
            
            <div class="form-group">
                <div class="controls">
                    <a href="{{ url('/administrador/categorys') }}" class="btn btn-secondary">Cancelar</a>
                    <input class="btn btn-success" type="submit" name="send" id="send" value="Grabar"/>
                </div>
            </div>             
        </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.col -->

    
@endsection
<script type="text/javascript" src="{{asset('js/validacioncategory.js')}}"></script>

