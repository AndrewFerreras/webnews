
@extends('layouts.admin')

@section('main_content')

<h1 class="mt-4">Categorias</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">En este recurso puedes visualizar los datos de una categoria en especifico</li>
</ol>

<div class="col-sm-6">
    <h4 class="m-0 text-dark">{{$categorys[0]->name}}</h4>
  </div><!-- /.col -->
  <br>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
           Datos de Categoria
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <table id="users_table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Datos</th>
                        <th>Información</th>
                    </tr>
                    </thead>
                
                    <tbody>
                        <tr>
                           
                            <td>  ID </td>
                            <td> {{ $categorys [0]->id }} </td>
                
                        </tr>
                        <tr>
                           
                            <td>  Nombre </td>
                            <td> {{ $categorys [0]->name }}  </td>
                            
                        </tr>
                        <tr>

                        <tr>
                    
                            @foreach ($users as $user)
                            @if ($user->id == $categorys [0]->create_at)
                                <td> Creado Por</td>
                                <td> {{ $user->name }}   </td>
                            @endif
                           @endforeach
                      </tr>
                      <tr>
                          <td>Creado</td>
                          <td>{{ $categorys [0]->created_at}}</td>
                      </tr>
                      <tr>
                    
                        @foreach ($users as $user)
                        @if ($user->id == $categorys [0]->update_at)
                            <td> Actualizado Por</td>
                            <td> {{ $user->name }}   </td>
                        @endif
                       @endforeach
                  </tr>
                  <tr>
                    <td>Actualizado</td>
                    <td>{{ $categorys [0]->updated_at}}</td>
                </tr>
                <tr>
                      
        </tbody>
    </table>

  
@endsection

