@extends('layouts.admin')

@section('main_content')

<h1 class="mt-4">Roles</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">En este recurso puede visualizar la Información de un rol</li>
</ol>

<div class="col-sm-6">
    <h4 class="m-0 text-dark">Rol {{$roles[0]->name}}</h4>
  </div><!-- /.col -->
  <div>
      <br>
  </div>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
          Detalle de Roles
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <table id="users_table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Datos</th>
                        <th>Información</th>
                    </tr>
                    </thead>
                
                    <tbody>
                        <tr>
                           
                            <td>  ID </td>
                            <td> {{ $roles [0]->id }} </td>
                
                        </tr>
                        <tr>
                           
                            <td>  Nombre </td>
                            <td> {{ $roles [0]->name }}  </td>
                            
                        </tr>
                        <tr>
                        
            

             <tr>
           
                <td>  Descripcion </td>
                <td> {{ $roles [0]->description}}   </td>
                
            </tr>
        </tbody>
    </table>

@endsection