@extends('layouts.admin')
@section('main_content')

    <h1 class="mt-4">Roles</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">En este recurso puedes ver los roles existentes y su descripcion</li>
    </ol>
    <div class="card mb-4">
      <div class="card-body">
        <a href="{{ url('/administrador/users/create') }}" class="btn btn-success btn-lg" role="button" aria-pressed="true">Crear Usuarios</a>
      </div>
  </div>
   
    
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
           Lista de Usuarios
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <table id="users_table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>descripcion</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($roles as $role)
                      <tr>
                          <td>{{ $role->id }}</td>
                          <td>{{ $role->name }}</td>
                          <td>{{ $role->description}} </td>
                          <td>
                              <div class="d-flex">
                                <ul class="list-inline center mx-auto justify-content-center m-0">
                                    <li class="list-inline-item">
                                      <a class="nav-link" 
                                        href="{{ url('/administrador/roles/' . $role->id ) }}"
                                        role="button"><i class="fas fa-book-open"></i></a>
                                    </li>
                                      {{-- <li class="list-inline-item">
                                        <a class="nav-link"
                                           href="{{ url('/admin/users/' . $user->id ) . '/delete' }}"
                                           role="button"><i class="fas fa-trash-alt"></i></a>
                                      </li> --}}
                                   
                                </ul>
                              
                              </div>
                          </td>
                      </tr>
                  @endforeach  
                </tbody>                     
              </table>
          </div>
          
        </div>
    </div>


@endsection
