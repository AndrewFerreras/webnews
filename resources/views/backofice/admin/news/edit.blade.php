@extends('layouts.admin')


@section('main_content')
<h1 class="mt-4">Noticias</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">En este recurso puedes Editar las noticias</li>
</ol>
<div class="card mb-4">
  
</div>
@foreach ($errors->all() as $error)
<div class="alert alert-danger alert-dismissible fade show" role="alert" id="error" >
  <strong>Error! </strong> {{ $error }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endforeach

  
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
          <h2>Editar Noticia </h2>
      </div>
      <div class="card-body">
        <form role="form" id="formnews" name="news" method="post" action="{{ url('/administrador/news/'. $news[0]->id) }}">
            @method('PUT')
            @csrf

            <input type="hidden" value="{{$news[0]->id}}" name="id" id="id">
            <div class="form-group">
                <div class="controls">
                    <label for="name"> Titulo de la noticia: </label>
                    <input class="form-control"  type="text" name="title" id="title" placeholder="Introducir Titulo."
                    value="{{$news[0]->title}}"/>
                   
                </div>
            </div>
            
            <div class="form-group">
                <label for="Category"> Categoria: </label>
                <select name="Category" id="Category" class="form-control" value="">
                    <option value="r"> -- Seleccionar -- </option>
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}" >{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="Urlphoto">URL de Imagen:</label>
                <input type="text" class="form-control" name="Urlphoto" id="Urlphoto" value="{{$news[0]->image}}">

            </div>

            <div class="form-group">
                <div class="controls">
                    <label for="Article"> Articulo: </label>
                    <textarea name="Article" id="Article" cols="30" rows="10" >
                        {!!$news[0]->body!!}
                    </textarea>
                </div>
            </div>
            
            <div class="form-group">
                <div class="controls">
                    <a href="{{ url('/administrador/news') }}" class="btn btn-secondary">Cancelar</a>
                    <input class="btn btn-success" type="submit" name="send" id="send" value="Grabar"/>
                </div>
            </div>             
        </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.col -->


<script src="{{asset('js/validationNewsForm.js')}}"></script>
<script type="text/javascript" src="{{asset('plugin/ckeditor5-build-classic/ckeditor.js')}}"></script>            
            <script >
                ClassicEditor.create( document.querySelector( '#Article' ) ).catch( error => {
                        console.error( error );
                    } );

                    
            </script>
            
  
@endsection

