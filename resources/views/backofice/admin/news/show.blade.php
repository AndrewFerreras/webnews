
@extends('layouts.admin')

@section('main_content')

<h1 class="mt-4">Noticias</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">En este recurso puedes visualizar los datos de una Noticia en especifico</li>
</ol>

<div class="col-sm-6">
    <h4 class="m-0 text-dark">{{$news[0]->title}}</h4>
  </div><!-- /.col -->
  <br>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
           Datos de Noticia
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <table id="news_table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Datos</th>
                        <th>Información</th>
                    </tr>
                    </thead>
                
                    <tbody>
                        <tr>
                           
                            <td>  ID </td>
                            <td> {{ $news [0]->id }} </td>
                
                        </tr>
                        <tr>
                           
                            <td>  Title </td>
                            <td> {{ $news [0]->title }}  </td>
                            
                        </tr>
                        <tr>
                          @foreach ($categories as $category)
                @if ($category->id == $news [0]->categoria)
                    <td> Categoría</td>
                    <td> {{ $category->name }}   </td>
                @endif
            @endforeach
            

             <tr>
           
                <td>  Url Image </td>
                <td> {{ $news [0]->image }}   </td>
                
            </tr>

            <tr>
           
                <td>  Autor</td>
                @foreach ($users as $user)
                    @if ( $user->id==$news[0]->autor)
                        <td>{{$user->name}}</td>
                    @endif
                @endforeach
               
            </tr>
            <tr>
                <td>  Creado </td>
                <td> {{ $news [0]->created_at }}   </td>
            </tr>

            <tr>
           
                <td>  Editador por</td>
                @foreach ($users as $user)
                    @if ( $user->id==$news[0]->update_for )
                        <td>{{$user->name}}</td>
                    @endif
                @endforeach
               
            </tr>
            <tr>
                <td>  Editado </td>
                <td> {{ $news [0]->updated_at }}   </td>
            </tr>
        </tbody>
    </table>

@endsection


