@extends('layouts.admin')


@section('main_content')
<h1 class="mt-4">Usuarios</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">En este recurso puedes crear nuevos usuarios administradores o editores</li>
</ol>
<div class="card mb-4">
  
</div>
@foreach ($errors->all() as $error)
<div class="alert alert-danger alert-dismissible fade show" role="alert" id="error" >
  <strong>Error! </strong> {{ $error }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endforeach

  
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
          <h2>Crear Usuario </h2>
      </div>
      <div class="card-body">
        <form role="form" id="formusers" name="users" method="post" action="{{ url('/administrador/users') }}">
            @method('POST')
            @csrf
            @include('backofice.admin.users.fields')       
            <div class="form-group">
                <div class="controls">
                    <a href="{{ url('/administrador/users') }}" class="btn btn-secondary">Cancelar</a>
                    <input class="btn btn-success" type="submit" name="send" id="send" value="Grabar"/>
                </div>
            </div>             
        </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.col -->

    
@endsection

