@extends('layouts.admin')


@section('main_content')
<h1 class="mt-4">Usuarios</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">En este recurso puedes editar los usuarios ya registrados en la plataforma</li>
</ol>
<div class="card mb-4">
  
</div>
@foreach ($errors->all() as $error)
<div class="alert alert-danger alert-dismissible fade show" role="alert" id="error" >
  <strong>Error! </strong> {{ $error }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endforeach

  
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
          <h2>Crear Usuario </h2>
      </div>
      <div class="card-body">
        <form role="form" id="formusers" name="users" method="post" action="{{ url('/administrador/users/'. $users[0]->id) }}">
            @method('PATCH')
            @csrf
           
 <input type="hidden" name="id" id="id" value="{{ $users[0]->id }}">
<div class="form-group">
    <div class="controls">
        <label for="name"> Nombre de Usuario: </label>
        <input class="form-control"  type="text" name="name" id="name" placeholder="Introducir usuario."
        value="{{ $users[0]->name }}"/>
       
    </div>
</div>

<div class="form-group">
    <div class="controls">
        <label for="email"> Email: </label>
        <input class="form-control"  type="text" name="email" id="email" placeholder="Introducir correo."
        value="{{  $users[0]->email }}" />
       
    </div>
</div>
 
<div class="form-group">
    <label for="role"> Rol: </label>
    <select name="role" id="role" class="form-control" value="">
        <option value="r"> -- Seleccionar -- </option>
        @foreach ($roles as $role)
            <option value="{{$role->id}}" >{{$role->name}}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <div class="controls">
        <label for="password"> Contraseña: </label>
        <input class="form-control"  type="password" name="password" id="password" minlength="8" maxlength="16" 
        placeholder="Introducir contraseña." />
        
    </div>
</div>

<div class="form-group">
    <div class="controls">
        <label for="pass2"> Repetir Contraseña: </label>
        <input class="form-control"  type="password" name="pass2" id="pass2" minlength="8" maxlength="16" 
        placeholder="Repetir contraseña." />
        
    </div>
</div>  
            <div class="form-group">
                <div class="controls">
                    <a href="{{ url('/administrador/users/') }}" class="btn btn-secondary">Cancelar</a>
                    <input class="btn btn-success" type="submit" name="send" id="send" value="Grabar"/>
                </div>
            </div>             
        </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.col -->

    
@endsection