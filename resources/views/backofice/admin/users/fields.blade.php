
<div class="form-group">
    <div class="controls">
        <label for="name"> Nombre de Usuario: </label>
        <input class="form-control"  type="text" name="name" id="name" placeholder="Introducir usuario."
        value="{{ old('name') }}"/>
       
    </div>
</div>

<div class="form-group">
    <div class="controls">
        <label for="email"> Email: </label>
        <input class="form-control"  type="text" name="email" id="email" placeholder="Introducir correo."
        value="{{ old('email') }}" />
       
    </div>
</div>
 
<div class="form-group">
    <label for="role"> Rol: </label>
    <select name="role" id="role" class="form-control" value="">
        <option value="r"> -- Seleccionar -- </option>
        @foreach ($roles as $role)
            <option value="{{$role->id}}" >{{$role->name}}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <div class="controls">
        <label for="password"> Contraseña: </label>
        <input class="form-control"  type="password" name="password" id="password" minlength="8" maxlength="16" 
        placeholder="Introducir contraseña." />
        
    </div>
</div>

<div class="form-group">
    <div class="controls">
        <label for="pass2"> Repetir Contraseña: </label>
        <input class="form-control"  type="password" name="pass2" id="pass2" minlength="8" maxlength="16" 
        placeholder="Repetir contraseña." />
        
    </div>
</div>