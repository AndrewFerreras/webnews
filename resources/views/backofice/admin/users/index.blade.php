@extends('layouts.admin')
@section('main_content')

    <h1 class="mt-4">Usuarios</h1>
    @if(session()->has('flash_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Excelente!</strong> {{ session()->get('flash_message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">En este recurso puedes crear nuevos usuarios administradores o editores</li>
    </ol>
    <div class="card mb-4">
      <div class="card-body">
        <a href="{{ url('/administrador/users/create') }}" class="btn btn-success btn-lg" role="button" aria-pressed="true">Crear Usuarios</a>
      </div>
  </div>
   
    
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
           Lista de Usuarios
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <table id="users_table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Correo electrónico</th>
                        <th>Rol</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($users as $user)
                      <tr>
                          <td>{{ $user->id }}</td>
                          <td>{{ $user->name }}</td>
                          <td>{{ $user->email}} </td>
                          <td>{{ $user->role}} </td>
                          <td>
                              <div class="d-flex">
                                <ul class="list-inline center mx-auto justify-content-center m-0">
                                    <li class="list-inline-item">
                                      <a class="nav-link" 
                                        href="{{ url('/administrador/users/' . $user->id ) }}"
                                        role="button"><i class="fas fa-book-open"></i></a>
                                    </li>
                                    <li class="list-inline-item">
                                      <a class="nav-link"
                                        href="{{ url('/administrador/users/' . $user->id ) . '/edit' }}"
                                        role="button"><i class="fas fa-edit"></i></a>
                                       
                                       
                                        <li class="list-inline-item">
                                          <a class="nav-link" href="#" role="button"
                                             onclick="deleteModelRecord({{$user->id}})"><i
                                                  class="fas fa-trash-alt"></i></a>
                                          <pre delete-dialog-model="deleteModelRecord" class="d-none">

                                  <form id="deleteModelRecord" name="delteModelRecord"
                                        action="{{ url('/administrador/users/')}}" method="POST">
                                      @method('DELETE')
                                      @csrf
                                  </form>


                                  <script>

                                      function deleteModelRecord (id) {

                                          url = $('#deleteModelRecord').attr('action') + "/" + id;

                                          Swal.fire({
                                              title: '¿Estás seguro que deseas eliminar este registro?',
                                              text: "La acción no podrá ser revertida!",
                                              icon: 'warning',
                                              showCancelButton: true,
                                              confirmButtonColor: '#3085d6',
                                              cancelButtonColor: '#d33',
                                              confirmButtonText: 'Sí, eliminarlo!',
                                              cancelButtonText: 'Cancelar',
                                          }).then((result) => {
                                              if (result.value) {

                                                  $('#deleteModelRecord').attr('action', url).submit();

                                              }
                                          });
                                      }


                                  </script>
                                  </pre>
                                      </li>


                                </ul>
                              
                              </div>
                          </td>
                      </tr>
                  @endforeach  
                </tbody>                     
              </table>
          </div>
          
        </div>
    </div>
    <script>
      document.addEventListener("DOMContentLoaded", function () {
  
          $('#users_table').DataTable({
              "responsive": true,
              "autoWidth": false,
          });
  
      });
      </script>
  
  

@endsection
