
@extends('layouts.admin')

@section('main_content')

<h1 class="mt-4">Usuarios</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">En este recurso puedes visualizar los datos de un usuario en especifico</li>
</ol>

<div class="col-sm-6">
    <h4 class="m-0 text-dark">{{$users[0]->name}}</h4>
  </div><!-- /.col -->
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
           Lista de Usuarios
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <table id="users_table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Datos</th>
                        <th>Información</th>
                    </tr>
                    </thead>
                
                    <tbody>
                        <tr>
                           
                            <td>  ID </td>
                            <td> {{ $users [0]->id }} </td>
                
                        </tr>
                        <tr>
                           
                            <td>  Nombre </td>
                            <td> {{ $users [0]->name }}  </td>
                            
                        </tr>
                        <tr>
                          {{-- @foreach ($categories as $category)
                @if ($category->id == $news [0]->category_id)
                    <td> Categoría</td>
                    <td> {{ $category->nombre }}   </td>
                @endif
            @endforeach
             --}}

             <tr>
           
                <td>  email </td>
                <td> {{ $users [0]->email }}   </td>
                
            </tr>
        </tbody>
    </table>

  
@endsection
