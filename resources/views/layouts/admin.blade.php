<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - WebNews</title>
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('/plugin/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        @include('backofice.admin.partial.nav-bar')
        @include('backofice.admin.partial.lef-sidebar')
     
        <div id="layoutSidenav_content">
           
            <main>
                <div class="container-fluid">
                @yield('main_content')  
                </div>
            </main>
        
            
    
            @include('backofice.admin.partial.footer')

        </div>

            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
            <script src="{{ asset('js/scripts.js') }}"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
            <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
            <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
            <script src="{{ asset('/plugin/sweetalert2/sweetalert2.min.js') }}"></script>
            <script src="{{ asset('js/validarusersform.js') }}"></script>
            <script src="{{ asset('/plugin/jquery-validation/jquery.validate.min.js') }}"></script>
            <script src="{{ asset('/plugin/jquery-validation/additional-methods.min.js') }}"></script>
            

    </body>
</html>